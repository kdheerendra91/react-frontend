"use strict";

var React = require('react');

var HomePage = React.createClass({
   render: function () {
       return (
           <div className='jumbotron'>
               <h1>This is test side</h1>
               <p>RectJS learning module</p>
           </div>
       );
   }
});

module.exports = HomePage;