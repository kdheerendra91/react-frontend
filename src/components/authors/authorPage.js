"use strict";

var React = require('react');

var authors = [
    {id: 'ab', name: 'Author 1'},
    {id: 'abc', name: 'Author 2'},
    {id: 'abc3', name: 'Author 3'}
];

var AuthorsPage = React.createClass({
    getInitialState: function () {
      return {
          authors: []
      };
    },
    componentWillMount: function () {
      this.setState({authors: authors});
    },
    render: function () {
        var createAuthorRow = function (author) {
                return (
                    <tr key={author.id}>
                        <td><a href={"/#authors/" + author.id}>author.id</a></td>
                        <td>{author.name}</td>
                    </tr>
                );
        };

        return (
            <div>
                <h1>Authors</h1>
                <table className="table">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                    </thead>
                    <tbody>
                        {this.state.authors.map(createAuthorRow, this)}
                    </tbody>
                </table>
            </div>
        );
    }
});

module.exports = AuthorsPage;